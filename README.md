# mongo crud utils

Simple CRUD helper for writing APIs on top of a mongo collection.

One important thing on ObjectIDs:

## IMPORTANT: Ids

We don't let mongo auto-generate the IDs using its native `ObjectID`.
It's confusing because you have to convert to/from it. 
We also automatically convert `_id` to `id` (when reading) and `id` to `_id` when writing.

We do automatically generate `_id` when creating if no `id` is supplied, 
but we convert it to a string before saving
so we never have to think about it again: `new ObjectID().toString()`.

Update/Patch operations omit the `id` or `_id` from the entity itself.

## release

```npm version patch```

Then push tags and reference in dependent project.

## usage

You can instantiate the class directly, or more likely, use it as a base to add more functionality:

```javascript
const { MongoCrud } = require('mongo-crud-utils');
const { MongoClient } = require('mongodb');

class MyService extends MongoCrud {
  constructor(db) {
    super(db, 'mysupercollection');
  }
}

const mongoUrl = 'mongodb://foo:123/mydb';
(async () => {
  const client = new MongoClient(mongoUrl, { useNewUrlParser: true });
  await client.connect();
  const crud = new MyService(client.db());
  console.log(crud.list())
})();

```

See tests for more usage examples.

## dev

### install

Have node 10 and `npm install`.

### test

```npm run dev:test```

### reformat code

```npm run reformat```

### why `'use strict';`?

Because it makes JS 
[less silly](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode) 
([summary](https://stackoverflow.com/a/27114400/1584651))
and running node with `--use-strict` forces it on third-party modules too, which can cause problems...
