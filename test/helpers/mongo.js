'use strict';

const { MongoMemoryServer } = require('mongodb-memory-server');
const { MongoClient } = require('mongodb');

module.exports.startMongo = async function() {
  const mongoServer = new MongoMemoryServer();
  const mongoUrl = await mongoServer.getConnectionString();
  const client = new MongoClient(mongoUrl, { useNewUrlParser: true });
  await client.connect();

  return {
    mongoServer,
    mongoClient: client,
  };
};
