'use strict';

const test = require('ava');

const _ = require('lodash');
const { startMongo } = require('./helpers/mongo');
const { MongoCrud, errors } = require('../index');

test.beforeEach('start mongo server', async t => {
  t.context = await startMongo();
  t.context.crud = new MongoCrud(t.context.mongoClient.db(), 'somecollection');
});

test('create', async t => {
  const res = await t.context.crud.create({ foo: 'bar' });
  t.is(res.foo, 'bar');
  t.is(res.id.length, 24);
});

test('create/get custom _id', async t => {
  const entity = { id: 'bar', a: 'b' };
  const cRes = await t.context.crud.create({ ...entity });
  t.deepEqual(cRes, entity);
  const gRes = await t.context.crud.get('bar');
  t.deepEqual(gRes, entity);
  const checkMongoRes = await t.context.mongoClient
    .db()
    .collection('somecollection')
    .find()
    .toArray();
  t.deepEqual(checkMongoRes, [
    {
      _id: 'bar',
      a: 'b',
    },
  ]);
});

test('create/get auto _id', async t => {
  const entity = { a: 'b' };
  const cRes = await t.context.crud.create({ ...entity });
  const { id } = cRes;
  const gRes = await t.context.crud.get(id);
  t.deepEqual(gRes, { id, ...entity });
  const checkMongoRes = await t.context.mongoClient
    .db()
    .collection('somecollection')
    .find()
    .toArray();
  t.is(id.length, 24);
  t.true(_.isString(id));
  t.deepEqual(checkMongoRes, [
    {
      _id: id,
      a: 'b',
    },
  ]);
});

test('patch', async t => {
  const cRes = await t.context.crud.create({ foo: 'bar', jim: 'bo' });
  const pRes = await t.context.crud.patch(cRes.id, { jim: 'jam' });
  const expected = { id: cRes.id, foo: 'bar', jim: 'jam' };
  t.deepEqual(pRes, expected);

  const gRes = await t.context.crud.get(cRes.id);
  t.deepEqual(gRes, expected);

  const fakeIdRes = await t.context.crud.patch('5c9250e736731d3d896e0bff', { jim: 'jam' });
  t.is(fakeIdRes, null);
});

test('update', async t => {
  const cRes = await t.context.crud.create({ foo: 'bar', jim: 'bo' });
  const pRes = await t.context.crud.update(cRes.id, { jim: 'jam' });
  const expected = { id: cRes.id, jim: 'jam' };
  t.deepEqual(pRes, expected);
  let gRes = await t.context.crud.get(cRes.id);
  t.deepEqual(gRes, expected);

  const fakeIdRes = await t.context.crud.update('5c9250e736731d3d896e0bff', { jim: 'jam' });
  t.is(fakeIdRes, null);
});

test('get by id', async t => {
  const cRes = await t.context.crud.create({ foo: 'bar' });
  const gRes = await t.context.crud.get(cRes.id);
  t.deepEqual(gRes, { id: cRes.id, foo: 'bar' });

  const fakeIdRes = await t.context.crud.get('5c9250e736731d3d896e0bff');
  t.is(fakeIdRes, null);
});

test('get by filter', async t => {
  await t.context.crud.create({ foo: 'bar' });
  const cRes = await t.context.crud.create({ foo: 'bar2' });
  const gRes = await t.context.crud.get({ foo: 'bar2' });
  t.deepEqual(gRes, { id: cRes.id, foo: 'bar2' });
  const noRes = await t.context.crud.get({ foo: 'bar3' });
  t.is(noRes, null);
});

test('get by WRONG', async t => {
  await t.throwsAsync(
    async () => {
      await t.context.crud.get(null);
    },
    { instanceOf: TypeError, message: 'Invalid argument must be id or filter object, not: null' },
  );
});

test('unknown mongo error', async t => {
  await t.throwsAsync(
    async () => {
      const crud = t.context.crud;
      crud.collection = {
        insertOne: () => Promise.reject(Error('something odd')),
      };
      await crud.create('5c9250e736731d3d896e0bff');
    },
    {
      instanceOf: errors.MongoError,
      message: 'Error: something odd',
    },
  );
});

test('un-parseable duplicate error', async t => {
  await t.throwsAsync(
    async () => {
      const crud = t.context.crud;
      crud.collection = {
        insertOne: () => Promise.reject({ code: 11000, errmsg: 'something odder' }),
      };
      await crud.create('5c9250e736731d3d896e0bff');
    },
    {
      instanceOf: errors.MongoConstraintError,
      message: 'Duplicate value: "UNKNOWN"',
    },
  );
});

test('delete', async t => {
  const cRes = await t.context.crud.create({ foo: 'bar' });
  const dRes = await t.context.crud.delete(cRes.id);
  const gRes = await t.context.crud.get(cRes.id);
  t.is(gRes, null);
  t.is(dRes.result.ok, 1);
});

test('list', async t => {
  const cRes1 = await t.context.crud.create({ foo: 'bar' });
  const cRes2 = await t.context.crud.create({ foo: 'bar2' });
  const lRes = await t.context.crud.list();
  t.deepEqual(lRes, [{ foo: 'bar', id: cRes1.id }, { foo: 'bar2', id: cRes2.id }]);

  const fRes = await t.context.crud.list({ foo: 'bar' });
  t.deepEqual(fRes, [{ foo: 'bar', id: cRes1.id }]);
});

test('duplicate indexes', async t => {
  await t.context.mongoClient
    .db()
    .collection('somecollection')
    .createIndex({ foo: 1 }, { unique: true });
  await t.context.crud.create({ foo: 'bar' });
  await t.throwsAsync(
    async () => {
      await t.context.crud.create({ foo: 'bar' });
    },
    { instanceOf: errors.MongoConstraintError, message: 'Duplicate value: "{ : "bar" }"' },
  );
  const { id } = await t.context.crud.create({ foo: 'bar2' });
  await t.throwsAsync(
    async () => {
      await t.context.crud.patch(id, { foo: 'bar' });
    },
    { instanceOf: errors.MongoConstraintError, message: 'Duplicate value: "{ : "bar" }"' },
  );
  await t.throwsAsync(
    async () => {
      await t.context.crud.update(id, { foo: 'bar' });
    },
    { instanceOf: errors.MongoConstraintError, message: 'Duplicate value: "{ : "bar" }"' },
  );
});

test('more duplicate indexes', async t => {
  await t.context.mongoClient
    .db()
    .collection('somecollection')
    .createIndex({ foo: 1, bar: 2 }, { unique: true });
  await t.context.crud.create({ foo: 'bar', bar: 'foo' });
  await t.throwsAsync(
    async () => {
      await t.context.crud.create({ foo: 'bar', bar: 'foo' });
    },
    { instanceOf: errors.MongoConstraintError, message: 'Duplicate value: "{ : "bar", : "foo" }"' },
  );
});

test.afterEach.always('cleanup mongo', async t => {
  if (t.context.mongoServer) {
    await t.context.mongoServer.stop();
  }
});
