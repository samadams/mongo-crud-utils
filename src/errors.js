'use strict';

class MongoError extends Error {}
class MongoConstraintError extends MongoError {}

module.exports.MongoConstraintError = MongoConstraintError;
module.exports.MongoError = MongoError;
