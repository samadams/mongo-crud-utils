'use strict';

const _ = require('lodash');
const { mongoToId } = require('./utils');
const ObjectID = require('mongodb').ObjectID;
const { MongoConstraintError, MongoError } = require('./errors');
const DUPLICATE_KEY_ERROR_CODE = 11000;

const parseMongoError = e => {
  if (e.code === DUPLICATE_KEY_ERROR_CODE) {
    const match = e.errmsg.match('dup key: ({.*})');
    const duplicateValue = match ? match[1] : 'UNKNOWN';
    throw new MongoConstraintError(`Duplicate value: "${duplicateValue}"`);
  }
  throw new MongoError(e.toString());
};

module.exports = class MongoCrud {
  constructor(db, collection) {
    this.collection = db.collection(collection);
  }

  async create(entity) {
    let writeResult;
    try {
      // mongo object id is strange - why use a special type when a string is accepted and can be unique?
      // to simplify all the logic we assume `id` is always a primary key and use it directly as the `_id`
      // or we generate a new ObjectID and immediately cast it as a string.
      const { id: _id = new ObjectID().toString(), ...other } = entity;
      writeResult = await this.collection.insertOne({ _id, ...other });
    } catch (e) {
      parseMongoError(e);
    }
    const { insertedId } = writeResult;
    return { id: _.toString(insertedId), ...entity };
  }

  async get(idOrFilter) {
    let entity;
    if (_.isObject(idOrFilter)) {
      entity = await this.collection.findOne(idOrFilter);
    } else if (_.isString(idOrFilter)) {
      entity = await this.collection.findOne({ _id: idOrFilter });
    } else {
      throw TypeError(`Invalid argument must be id or filter object, not: ${idOrFilter}`);
    }
    return entity ? mongoToId(entity) : null;
  }

  async list(filter = {}) {
    const entities = await this.collection.find(filter).toArray();
    return entities.map(entity => mongoToId(entity));
  }

  async delete(id) {
    return await this.collection.deleteOne({ _id: id });
  }

  async patch(id, updates) {
    let res;
    try {
      // update with $set actually patches
      res = await this.collection.updateOne({ _id: id }, { $set: _.omit(updates, ['id', '_id']) });
    } catch (e) {
      parseMongoError(e);
    }

    if (!res.matchedCount) {
      return null;
    }
    return await this.get(id);
  }

  async update(id, replacement) {
    let res;
    replacement = _.omit(replacement, ['id', '_id']);
    try {
      res = await this.collection.replaceOne({ _id: id }, replacement);
    } catch (e) {
      parseMongoError(e);
    }
    if (!res.matchedCount) {
      return null;
    }
    return { id, ...replacement };
  }
};
