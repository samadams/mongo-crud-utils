'use strict';

module.exports.mongoToId = obj => {
  const { _id: id, ...other } = obj;
  return { id, ...other };
};
