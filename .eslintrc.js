module.exports = {
  plugins: [
    "prettier",
  ],
  "rules": {
    "prettier/prettier": "error",
    strict: ["error", "global"]
  },
  parserOptions: {
    ecmaVersion: 2018, // Allows for the parsing of modern ECMAScript features
  },
};
